# README #

Framework: QT 5.5

Those are the codes I've built for my thesis at Salvador University (UNIFACS). The name of this tool is Knowledge HUB (KHUB) and its purpose is educational. The idea is to share references (knowledge) between students inside or outside the classroom, thus helping students' learning or professors' teaching.

KHUB's usability synopsis:

1. Login
2. Join a study group
3. Type keywords to be searched (HTTP request to Google's engine)
4. Share good references for studying with others

### What is this repository for? ###

* Codes for my Thesis developed during my bachelor degree in Computer Engineering
* Final Version

### How do I get set up? ###

* N/A

### Contribution guidelines ###

* N/A

### Who do I talk to? ###

* roliveira.victor@gmail.com

### Youtube ###

* KHUB: https://www.youtube.com/watch?v=hB-QcOBG8dY